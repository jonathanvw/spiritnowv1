/*jshint multistr: true */
/*jshint camelcase: false */
 /*jshint unused:false*/


var test, query, rRequestedVideo, requestedVideo, requestedVideoId, rPageNumber, str, PL, jsonData, finalQuery, getFlagshipVideo, myTemplateLoaded, onTemplateError, getVideosByTag, videoByTagResponse, getTopVideos, buildTopVideos, getVideoByTitle, loadRelatedVideoResponse, searchGenResponse, dynaLoad, experienceID, evt, callback, player, modVP, modExp, modCon, brightcove;

/*************************************************** UTILITY FUNCTIONS ***************************************************/
function addScriptTag(id, url, callback, args)
{
    'use strict';
    var scriptTag = document.createElement( 'script' );
    scriptTag.setAttribute( 'type', 'text/javascript' );
    scriptTag.setAttribute( 'charset', 'utf-8' );
    scriptTag.setAttribute( 'src', url + '&callback=' + callback);
    scriptTag.setAttribute( 'id', id);
    var head = document.getElementsByTagName( 'head' ).item( 0 );
    head.appendChild( scriptTag );
}

/*************************************************** FLAGSHIP VIDEO ***************************************************/

function getVideosByTag( tag, callback ){
    'use strict';
    var mediaAPI = 'https://api.brightcove.com/services/library?callback=?';
    $.getJSON( mediaAPI,{
      command: 'search_videos',
      video_fields: 'id,shortDescription,name,videoStillURL,playsTrailingWeek',
      sort_by: 'CREATION_DATE:DESC',
      all: 'tag:' + tag,
      media_delivery: 'http',
      token: '73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..',
      format: 'json'
    })
    .done(function( data ) {
        // console.log( data.items[0].id );
        callback( data );
    });
}

function getVideoById( Id, callback ){
    'use strict';
    var mediaAPI = 'https://api.brightcove.com/services/library?callback=?';
    $.getJSON( mediaAPI,{
      command: 'find_video_by_id',
      video_fields: 'id,shortDescription,name,videoStillURL,playsTrailingWeek',
      video_id: Id,
      media_delivery: 'http',
      token: '73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..',
      format: 'json'
    })
    .done(function( data ) {
        //console.log( data );
        callback( data );
    });
}

/*************************************************** VIDEO DESCRIPTIONS ***************************************************/
function loadShortDescription(string, requestedVideoId){
    'use strict';
    if(requestedVideoId.shortDescription !== null) //If Short Description is defined us it in label.
    {
        string += '<p>' + requestedVideoId.shortDescription  + '</p>';
        return string;
    }
}

function loadFeatureVid( jsonData ){
    var videoId, videoName;
    if( typeof jsonData === 'object' ){
      if( jsonData.hasOwnProperty( 'items' ) ){
        videoId = jsonData.items[0].id;
        videoName = jsonData.items[0].name;
      }else{
        videoId = jsonData.id;
        videoName = jsonData.name;
      }
    }else{
      console.log( "ERROR: value passed to loadFeatureVid() was not a valid object." );
      return false;
    }
    var caption = '<a href="https://spiritube.web.spiritaero.com/player.htm#V=' + videoId + '"><i class="fa fa-link"></i> ' + videoName + '</a>';
    $( '.featured .caption' ).html( caption);
    try{
      requestedVideo = videoId;
      loadVideo( videoId );
    }
    catch(e){
      console.log(e);
    }

}

function getVideoPlaylist( jsonData ){
  var playlistHtml = '';
  for ( var i = 0; i < jsonData.items.length; i++ ){
    var video = jsonData.items[i];
    playlistHtml += buildPlaylistItemDom( video );
  }
  $('.video-list ul').html( playlistHtml );
}

function buildPlaylistItemDom( videoObj ){
  var htmlOut = '';
  htmlOut += '\
  <li>\
    <a href="" onclick="getVideoById(\'' + videoObj.id + '\', loadFeatureVid ); return false;">\
      <img class="img-responsive" src="' + videoObj.videoStillURL + '" alt="">\
      <p class="caption">' + videoObj.name + '</p>\
    </a>\
  </li>';
  return htmlOut;

}


