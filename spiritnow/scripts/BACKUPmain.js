/*jshint strict: true */
/*jshint multistr: true */
/*jshint camelcase: false */
/*jshint unused:false*/
/*global getVideosByTag, loadFeatureVid, getVideosByTag, getVideoPlaylist*/


var test, htmlString, getVideoByIdTag;

$(document).ready( function() {
  'use strict';
  /* automatically organize and compact the article layouts AFTER images are loaded */
  $('.brick-wall').imagesLoaded()
    .always( function(){
      $('.brick-wall').packery({
        itemSelector: '.brick-item'
      });
  });

  /* Initialize the image slider */
  $('.owl-carousel').owlCarousel({
    items : 1,
    loop : true,
    autoplay : false,
    autoplayHoverPause: true,
    dots: false,
    video: true,
    lazyLoad: true,
    nav: true
  });

  /* MODULE: load Spirit Newslines from internal RSS Feed */
  var newslinesUrl = 'http://cms.web.spiritaero.com/WorkArea/blogs/blogrss.aspx?blog=599';
  var getNewslines = function( inputUrl ){
    var htmlOut = '';
    $.ajax({
      url: inputUrl,
      type: 'get',
      dataType: 'xml',
      async: false,
      success: function (data) {
        var count = 1;
        $(data).find('item').each(function () { // or 'item' or whatever suits your feed
          //test = $($(this)[0].xml)[10].nodeName;
          //test = $($(this)[0].xml)[10];
          if ( count > 3  ){
            return false;
          }else{
            count++;
          }
          var el = $(this);
          htmlOut += '<h4><a href=\''+ el.find('link').text() + '\' >' + el.find('title').text() + '</a></h4>';
          try{
            htmlString = el.children().last().html();
            htmlString = htmlString.replace( '<![CDATA[', '').replace(']]>', '' );
            var paragraphs = $(htmlString).filter('p');
            htmlOut += '<p>';
            htmlOut += paragraphs[1].innerHTML;
            htmlOut += '</p>';
          }
          catch(e){
          }
          // Now try for IE workaround
          try{
            var $that = $(this);
            //test = $($(this)[0].xml)[10].nodeName;
            var xml2;
            for(i = 0; i < 3; i++){
              xml2 = $($(this)[i].xml);
              for( j = 0; j < xml2.length; j++ ){
                if( $( $( this )[i].xml )[j].nodeName == "encoded" ){
                  htmlString = $( $( this )[i].xml )[j].innerHTML;
                  htmlString = htmlString.replace( '<![CDATA[', '').replace(']]>', '' );
                  var paragraphs = $(htmlString).filter('p');
                  test = paragraphs;
                  htmlOut += '<p>';
                  htmlOut += paragraphs[2].innerHTML;
                  htmlOut += '</p>';
                }
              }
            }
          }
          catch(e){
          }

        });
      },
      error: function(){
        htmlOut = '<h4><a href="http://inside.spiritaero.com/News/Newslines/Friday,_June_12,_2015.aspx?blogid=599">Friday, June 12, 2015</a></h4><p><b>Walk with Wildlife cancelled<br>New shipping address for Oklahoma Business Unit <br> Mail Center relocation<br> Climate control during summer months</b></p><h4><a href="http://inside.spiritaero.com/News/Newslines/Tuesday,_June_9,_2015.aspx?blogid=599">Tuesday, June 9, 2015</a></h4><p><b>Quality culture survey<br> “Show Your 10” with a Photo<br> Program Management PM201<br> Access to Reed Group website for leave</b> </p><h4><a href="http://inside.spiritaero.com/News/Newslines/Friday,_June_5,_2015.aspx?blogid=599">Friday, June 5, 2015</a></h4><p><b>AS9100 &amp; SAI Global Audit: June 8-12<br>Volunteers needed for Walk with Wildlife</b></p>';
      }
    });
    return htmlOut;
  };

  $( '.newslines-list' ).prepend( getNewslines( newslinesUrl ) );

  /* set active class in a list of links */
  var setActiveLink = function( $object ){
    $object.parent().siblings('li').children('a').removeClass('active');
    $object.addClass('active');
  };

  /* load new press release content on btn click */
  $('.btn-pr').click(function(e){
    e.preventDefault();
    setActiveLink( $(this) );
    var prUrl = $(this).attr('data-rss');
    var target = $('.press-release-content');
    $('.press-release-content').slideUp();
    target.html('');
    target.rss(prUrl,{
      entryTemplate: '<li><a href="{url}">{title}</a></li>'
    });
    $('.press-release-content').slideDown();
    return false;
  });

  /* Initialize default press release data on page load */
   $('.btn-pr').first().click();
  /* jshint ignore:end */

  /* Load the twitter feeds */
  /* jshint ignore:start */
  var fetchTwitter = function( twitterId, targetId ){
    var twitterCallObj = {
      /* defaults to spiritaero id */
      'id' : twitterId ,
      'domId' : targetId,
      'showImages' : true,
      'showTime' : false,
      'maxTweets' : 10,
      'enableLinks' : true,
      'showPermalinks' : false
    };
    twitterFetcher.fetch(twitterCallObj);
  };


  /* load new twitter content on btn click */
  $('.btn-twitter').click(function(e){
    e.preventDefault();
    setActiveLink( $(this) );
    $(this).parent().siblings('li').children('a').removeClass('active');
    $(this).addClass('active');
    var twitterAppId = $(this).attr('data-twitterId');
    var target = 'tweet-list';
    fetchTwitter( twitterAppId, target );
    $('.twitter-content').scrollTop( 0 );
    return false;
  });

  /* Initialize default twitter data on page load */
  fetchTwitter( '476078419180924928', 'tweet-list' );
  /* jshint ignore:end */

  /* Initialize RSS NewsFeeds */
  $('.boeing-feed').rss('http://boeing.mediaroom.com/news-releases-statements?pagetemplate=rss&category=786',{
    entryTemplate: '<li><a href="{url}">{title}</a><br />{shortBodyPlain}</li>'
  });
  $('.airbus-feed').rss('http://www.airbus.com/newsevents/rss/all_airbus_families_news.xml',{
    entryTemplate: '<li><a href="{url}">{title}</a><br />{shortBodyPlain}</li>'
  });
  $('.other-feed').rss('http://pipes.yahoo.com/pipes/pipe.run?_id=d0b5d22359b492a1c29aad26faacc28d&_render=rss',{
    entryTemplate: '<li><a href="{url}">{title}</a><br />{shortBodyPlain}</li>'
  });

  /* Initialize getDate */
  if( $('.date-wrapper').length > 0 ){
    /* jshint ignore:start */
    printDate( $('.date-wrapper') );
    /* jshint ignore:end */
  }

  /* Initialize Stock Data */
  if( $('.stock-wrapper').length > 0 ){
    /* jshint ignore:start */
    getStockData( 'SPR' , $('.stock-wrapper') );
    /* jshint ignore:end */
  }
  if( $('.stock-wrapper').length > 0 ){
    /* jshint ignore:start */
    getStock( 'SPR' , $('.stock-quote') );
    /* jshint ignore:end */
  }

  /* Initialize Highlighted Video */
  try{
    if( $('.spirit-videos-wrapper .featured').length > 0 ){
      getVideosByTag( 'PL-SpiritNow', loadFeatureVid, 0);
      getVideosByTag( 'PL-SpiritNow', getVideoPlaylist, 1 );
    }
  }
  catch(e){
    console.log(e + ": main.js");
  }
});
