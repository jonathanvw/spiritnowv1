  /* jshint ignore:start */
  function getRss(rssURL, limit, $targetObj) {
    'use strict';
    var url = '//query.yahooapis.com/v1/public/yql';
    var data = encodeURIComponent('select * from xml where url = "' + rssURL + '"');
    var output = '';
    //console.log(url + '?q=' + data + '&format=json&env=store://datatables.org/alltableswithkeys&callback=?');
    $.getJSON(url, 'q=' + data + '&format=json&env=store://datatables.org/alltableswithkeys&callback=?')
    .done(function (data) {
        console.log(data);
        //test=data;
        var items = data.query.results.rss.channel.item;
        var htmlOut = "";
        for ( var i =0; i < items.length && i < limit ; i++){
          var item = items[i];
          var thisUrl = item.url || item.link;
          var thisTitle = item.title.replace("<p>","").replace("</p>","");
          htmlOut += '<li><a href="' + thisUrl + '">' + thisTitle + '</a></li>';
        }
        $targetObj.html( htmlOut );
    });
  }
  /* jshint ignore:end */
