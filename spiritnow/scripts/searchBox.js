$(document).ready(function(){
  'use strict';
  var submitBtn = $('.searchbox-submit');
  var inputBox = $('.searchbox-input');
  var searchBox = $('.searchbox');
  var isOpen = false;
  submitBtn.click(function(){
    if(isOpen === false){
      searchBox.addClass('searchbox-open');
      inputBox.focus();
      isOpen = true;
      return false;
    } else {
      searchBox.removeClass('searchbox-open');
      inputBox.focusout();
      isOpen = false;
      if( inputBox.Value === ''  || inputBox.Value === undefined ){
        return false;
      }
    }
  });
  submitBtn.mouseup(function(){
    return false;
  });
  searchBox.mouseup(function(){
    return false;
  });
  $(document).mouseup(function(){
    if(isOpen === true){
      $('.searchbox-icon').css('display','block');
      submitBtn.click();
    }
  });
});

/* jshint ignore:start */
function buttonUp(){
  'use strict';
  var inputVal = $('.searchbox-input').val();
  inputVal = $.trim(inputVal).length;
}
/* jshint ignore:end */
