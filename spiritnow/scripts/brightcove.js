var myPlayer;

function loadFeatureVid( jsonData ){
    var videoId, videoName;
    if( typeof jsonData === 'object' ){
      if( jsonData.hasOwnProperty( 'items' ) ){
        videoId = jsonData.items[0].id;
        videoName = jsonData.items[0].name;
      }else{
        videoId = jsonData.id;
        videoName = jsonData.name;
      }
    }else{
      console.log( "ERROR: value passed to loadFeatureVid() was not a valid object." );
      return false;
    }
    var caption = '<a href="https://spiritube.web.spiritaero.com/player.htm#V=' + videoId + '"><i class="fa fa-link"></i> ' + videoName + '</a>';
    $( '.featured .caption' ).html( caption);
    try{
      myPlayer = videojs( 'featured-video' );
      myPlayer.catalog.getVideo( videoId, function( error, video ){
        myPlayer.catalog.load( video );
      });
    }
    catch(e){
      console.log(e);
    }

}

function getVideoPlaylist( jsonData ){
  var playlistHtml = '';
  for ( var i = 0; i < jsonData.items.length; i++ ){
    var video = jsonData.items[i];
    playlistHtml += buildPlaylistItemDom( video );
  }
  $('.video-list ul').html( playlistHtml );
}

function buildPlaylistItemDom( videoObj ){
  var htmlOut = '';
  htmlOut += '\
  <li>\
    <a href="" onclick="getVideoById(\'' + videoObj.id + '\', loadFeatureVid ); return false;">\
      <img class="img-responsive" src="' + videoObj.videoStillURL + '" alt="">\
      <p class="caption">' + videoObj.name + '</p>\
    </a>\
  </li>';
  return htmlOut;

}

$(document).ready(function(){
    getVideosByTag( 'PL-SpiritNow', getVideoPlaylist );
});

