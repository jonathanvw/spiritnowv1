  /* jshint ignore:start */
  function getStock(stock, $target) {
    'use strict';
    var url = '//query.yahooapis.com/v1/public/yql';
    var data = encodeURIComponent('select * from yahoo.finance.quotes where symbol in (\''+ stock +'\')');
    var output = '';
    $.getJSON(url, 'q=' + data + '&format=json&diagnostics=true&env=http://datatables.org/alltables.env&callback=?')
    .done(function (data) {
        try{
        output += data.query.results.quote.Symbol;
        output += ' ';
        output += data.query.results.quote.LastTradePriceOnly;
        if (data.query.results.quote.PercentChange.indexOf('+') !== -1) {
            output += ' <span class=\'text-success\'>';
        } else {
            output += ' <span class=\'text-error\'>';
        }
        output += data.query.results.quote.PercentChange;
        output += '</span>';
        $target.html(output);
        }
        catch(e){
          console.log( "error: getStock.js has an exception: " + e );
        }
    });
  }

  function getStockData(stock, $target) {
    'use strict';
    var url = '//query.yahooapis.com/v1/public/yql';
    var data = encodeURIComponent('select * from yahoo.finance.quotes where symbol in (\''+ stock +'\')');
    var output = '';
    $.getJSON(url, 'q=' + data + '&format=json&diagnostics=true&env=http://datatables.org/alltables.env&callback=?')
    .done(function (data) {
            output += '<h3 class="pad-thin">' + data.query.results.quote.Symbol + ' Stock : <span class="lighter">$' + data.query.results.quote.LastTradePriceOnly + '</span></h3>';
            output += '<div class="row">';
              output += '<div class="col-xs-6 col-sm-12 col-md-12 col-lg-6">';
                  output += '<table class="table table-responsive table-condensed table-no-border">';
                    output += '<tr>';
                      output += '<td><strong>Prev Close:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.PreviousClose + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>Open:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.Open + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>High:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.DaysHigh + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>Low:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.DaysLow + '</td>';
                    output += '</tr>';
                  output += '</table>';
              output += '</div>';
              output += '<div class="col-xs-6 col-sm-12 col-md-12 col-lg-6">';
                  output += '<table class="table table-responsive table-condensed table-no-border">';
                    output += '<tr>';
                      output += '<td><strong>52 Wk High:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.YearHigh + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>52 Wk Low:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.YearLow + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>Volume:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.Volume + '</td>';
                    output += '</tr>';
                    output += '<tr>';
                      output += '<td><strong>Avg Vol:</strong></td>';
                      output += '<td class="text-right">' + data.query.results.quote.AverageDailyVolume + '</td>';
                    output += '</tr>';
                  output += '</table>';
              output += '</div>';
            output += '</div>';
        $target.html(output);
    });
  }
  /* jshint ignore:end */
