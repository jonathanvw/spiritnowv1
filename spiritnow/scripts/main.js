/*jshint strict: true */
/*jshint multistr: true */
/*jshint camelcase: false */
/*jshint unused:false*/
/*global getVideosByTag, loadFeatureVid, getVideosByTag, getVideoPlaylist*/


var test, htmlString, getVideoByIdTag;

$(document).ready( function() {
  'use strict';
  /* automatically organize and compact the article layouts AFTER images are loaded */
  $('.brick-wall').imagesLoaded()
    .always( function(){
      $('.brick-wall').packery({
        itemSelector: '.brick-item',
        stamp: '.stamp'
      });
  });

  /* Initialize the image slider */
  $('.owl-carousel').owlCarousel({
    items : 1,
    loop : true,
    autoplay : true,
    autoplayHoverPause: true,
    dots: false,
    video: true,
    lazyLoad: true,
    nav: true
  });

  /* MODULE: load Spirit Newslines from internal RSS Feed */
  var newslinesUrl = '/WorkArea/blogs/blogrss.aspx?blog=599';
  var getNewslines = function( inputUrl ){
    var htmlOut = '';
    $.ajax({
      url: inputUrl,
      type: 'get',
      dataType: 'xml',
      async: false,
      success: function (data) {
        var count = 1;
        $(data).find('item').each(function () { // or 'item' or whatever suits your feed
          if ( count > 3  ){
            return false;
          }else{
            count++;
          }
          var el = $(this);
          var nlLink = el.find('link').text();
          htmlOut += '<h4><a href=\''+ nlLink + '\' >' + el.find('title').text() + '</a></h4>';
          var xml2 = el[0].childNodes;

          for( var j = 0; j < xml2.length; j++ ){
            if( xml2[j].nodeName.indexOf("content") == 0 ){
              htmlString = xml2[j].firstChild.data;
              var paragraphs = $(htmlString).filter('p');
              htmlOut += '<p><a href=\''+ nlLink + '\' >';
              htmlOut += paragraphs[0].innerHTML;
              htmlOut += '</a></p>';
            }
          }
        });
      },
      error: function(){
        htmlOut = '<h4>Failed to load Newslines Feed</h4>';
      }
    });
    return htmlOut;
  };

  $( '.newslines-list' ).prepend( getNewslines( newslinesUrl ) );

  /* set active class in a list of links */
  var setActiveLink = function( $object ){
    $object.parent().siblings('li').children('a').removeClass('active');
    $object.addClass('active');
  };

  /* load new press release content on btn click */
  $('.btn-pr').click(function(e){
    e.preventDefault();
    setActiveLink( $(this) );
    var prUrl = $(this).attr('data-rss');
    var target = $('.press-release-content');
    $('.press-release-content').slideUp();
    target.html('');
    getRss(prUrl, 6, $(".press-release-content"));
    $('.press-release-content').slideDown();
    return false;
  });

  /* Initialize default press release data on page load */
   $('.btn-pr').first().click();
  /* jshint ignore:end */

  /* Load the twitter feeds */
  /* jshint ignore:start */
  var fetchTwitter = function( twitterId, targetId ){
    var twitterCallObj = {
      /* defaults to spiritaero id */
      'id' : twitterId ,
      'domId' : targetId,
      'showImages' : true,
      'showTime' : false,
      'maxTweets' : 10,
      'enableLinks' : true,
      'showPermalinks' : false
    };
    twitterFetcher.fetch(twitterCallObj);
  };


  /* load new twitter content on btn click */
  $('.btn-twitter').click(function(e){
    e.preventDefault();
    setActiveLink( $(this) );
    $(this).parent().siblings('li').children('a').removeClass('active');
    $(this).addClass('active');
    var twitterAppId = $(this).attr('data-twitterId');
    var target = 'tweet-list';
    fetchTwitter( twitterAppId, target );
    $('.twitter-content').scrollTop( 0 );
    return false;
  });

  /* Initialize default twitter data on page load */
  fetchTwitter( '476078419180924928', 'tweet-list' );
  /* jshint ignore:end */

  /* Initialize RSS NewsFeeds */
  getRss("http://boeing.mediaroom.com/news-releases-statements?pagetemplate=rss&category=786", 6, $(".boeing-feed"));
  getRss("http://www.airbus.com/newsevents/rss/all_airbus_families_news.xml", 6, $(".airbus-feed"));

    var url = "https://query.yahooapis.com/v1/public/yql";
    var output = '';
    // The one below includes Bell feed which has issues
    //$.getJSON(url, "q=select%20channel.item.title%2Cchannel.item.link%2C%20channel.item.pubDate%2C%20channel.item.description%20from%20xml%20where%20url%20in%20('http%3A%2F%2Fwww.sikorsky.com%2F_layouts%2Ffeed.aspx%3Fxsl%3D1%26web%3D%252F%26page%3D5dd004f7-0c34-4e45-8c8f-35855dd6cc25%26wp%3D79c34605-4cf6-44b8-9b91-87a162f4c0b5%26pageurl%3D%252FPages%252FAboutSikorsky%252FPressReleaseRSS%252Easpx'%2C'http%3A%2F%2Ffeed43.com%2F8831706846421816.xml'%2C'https%3A%2F%2Fwww.mitsubishi.com%2Fe%2Frss%2Findex_e.xml'%2C'http%3A%2F%2Fwww.rolls-royce.com%2Frss%2Fpress-releases'%2C'http%3A%2F%2Fwww.bombardier.com%2Fservices%2Frss.en.bca.xml')%20%7C%20unique(field%3D%22channel.item.link%22)%20%20%20%7C%20sort(field%3D%22channel.item.pubDate%22%2C%20descending%3D%22true%22)" + '&format=json&env=store://datatables.org/alltableswithkeys&callback=?')
    $.getJSON(url, "q=select%20channel.item.title%2Cchannel.item.link%2C%20channel.item.pubDate%2C%20channel.item.description%20from%20xml%20where%20url%20in%20('http%3A%2F%2Fwww.sikorsky.com%2F_layouts%2Ffeed.aspx%3Fxsl%3D1%26web%3D%252F%26page%3D5dd004f7-0c34-4e45-8c8f-35855dd6cc25%26wp%3D79c34605-4cf6-44b8-9b91-87a162f4c0b5%26pageurl%3D%252FPages%252FAboutSikorsky%252FPressReleaseRSS%252Easpx'%2C'https%3A%2F%2Fwww.mitsubishi.com%2Fe%2Frss%2Findex_e.xml'%2C'http%3A%2F%2Fwww.rolls-royce.com%2Frss%2Fpress-releases'%2C'http%3A%2F%2Fwww.bombardier.com%2Fservices%2Frss.en.bca.xml')%20%7C%20unique(field%3D%22channel.item.link%22)%20%20%20%7C%20sort(field%3D%22channel.item.pubDate%22%2C%20descending%3D%22true%22)" + '&format=json&env=store://datatables.org/alltableswithkeys&callback=?')
    .done(function (data) {
        var items = data.query.results.rss;
        var htmlOut = "";
        for ( var i =0; i < items.length && i < 6; i++){
          var item = items[i].channel.item;
          htmlOut += '<li><a href="' + item.link + '">' + item.title + '</a></li>';
        }
        $(".other-feed").html( htmlOut );
    });


  /* Initialize getDate */
  if( $('.date-wrapper').length > 0 ){
    /* jshint ignore:start */
    printDate( $('.date-wrapper') );
    /* jshint ignore:end */
  }

  /* Initialize Stock Data */
  if( $('.stock-wrapper').length > 0 ){
    /* jshint ignore:start */
    getStockData( 'SPR' , $('.stock-wrapper') );
    /* jshint ignore:end */
  }
  if( $('.stock-wrapper').length > 0 ){
    /* jshint ignore:start */
    getStock( 'SPR' , $('.stock-quote') );
    /* jshint ignore:end */
  }

  /* Initialize Highlighted Video */
  try{
    if( $('.spirit-videos-wrapper .featured').length > 0 ){
      getVideosByTag( 'PL-SpiritNow', loadFeatureVid, 0);
      getVideosByTag( 'PL-SpiritNow', getVideoPlaylist, 1 );
    }
  }
  catch(e){
    console.log(e + ": main.js");
  }
});
