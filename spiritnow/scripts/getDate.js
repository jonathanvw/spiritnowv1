/*jshint strict: true */

/* jshint ignore:start */
var printDate = function($target){
  'use strict';
  var dt = new Date();
  $target.html( dt.toDateString() );
};
/* jshint ignore:end */
