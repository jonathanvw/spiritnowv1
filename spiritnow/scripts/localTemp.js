$(document).ready(function(){
  'use strict';
  var city,
      region,
      tempInfo;

  // if we have a place to input our local temperature proceed
  if ( $('.temperature-wrapper').length > 0 ){
    // Get location info based on ip address using third party service that returns json
    $.get('http://ipinfo.io', function (response) {
      city = response.city;
      region = response.region;
    }, 'jsonp')
    .done(function(){
      // Get weather info based on city/region using yahoo'ls yql api that retruns json
      $.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22' + city + '%2C%20' + region + '%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys', function (response) {
        tempInfo = response.query.results.channel.item.condition.temp + '&deg;' + response.query.results.channel.units.temperature;
      }, 'jsonp')
      .done(function(){
        // output final city and temp into wrapper span
        $('.temperature-wrapper').html( city + ': ' + tempInfo );
      });
    });
  }
});

/* jshint ignore:start */
/* jshint ignore:end */
