/*************************************************** UTILITY FUNCTIONS ***************************************************/
function addScriptTag(id, url, callback)
{
    var scriptTag = document.createElement("script");
    scriptTag.setAttribute("type", "text/javascript");
    scriptTag.setAttribute("charset", "utf-8");
    scriptTag.setAttribute("src", url + "&callback=" + callback);
    scriptTag.setAttribute("id", id);
    var head = document.getElementsByTagName("head").item(0);
    head.appendChild(scriptTag);

}

$.extend( //Extends Jquery to use a function that will read passed parameters in url and assign to variable
{
  getUrlVars: function(url)
  {
    var vars = [], hash;
    if(url==null)
    {
        var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
    }
    else
    {
        var hashes = url.slice(window.location.href.indexOf('#') + 1).split('&');
    }
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name)
  {
    return $.getUrlVars()[name];
  }
});

/*************************************************** PLAYER CALLBACK FUNCTIONS ***************************************************/
function myTemplateReady(experienceID)
{
}

function myTemplateLoaded(experienceID)
{
    $(document).ready(function()
    {
        player = brightcove.api.getExperience(experienceID);
        modVP = player.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
        modExp = player.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
        modCon = player.getModule(brightcove.api.modules.APIModules.CONTENT);
        modExp.addEventListener(brightcove.api.events.ExperienceEvent.TEMPLATE_READY, onTemplateReady);
    });
}

function onTemplateReady(evt)
{
    $(document).ready(function()
    {
        modVP.cueVideoByID(requestedVideo);
    });
}

function onTemplateError(event) {
}

function loadVideo(videoID)
{
    requestedVideo=videoID;
    modVP.cueVideoByID(requestedVideo);
}

/*************************************************** FLAGSHIP VIDEO ***************************************************/
function getFlagshipVideo()
{
    addScriptTag("flagshipVideoJS", "https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&video_fields=id&all=tag:flagship", "flagshipVideoResponse");
}

function flagshipVideoResponse(jsonData)
{
    requestedVideo=jsonData["items"][0].id;
}

/*************************************************** TRENDING VIDEOS ***************************************************/
function getTopVideos()
{
    addScriptTag("topVideosJS", "https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&video_fields=id,shortDescription,name,videoStillURL,playsTrailingWeek&sort_by=plays_trailing_week:DESC&page_size=3&none=tag:Flagship&none=tag:hidden&none=tag:ignoreTrends", "topVideoResponse");
}

function topVideoResponse(jsonData)
{
    var str = "";
    for (var i = 0; i<jsonData["items"].length; i++)
    {
        var video = jsonData["items"][i];
        str += '<div class="span4 popular-video-wrapper">\
                <a href="#myExperience" onClick="dynaLoad('+video.id+');return false;"><img src="' + video.videoStillURL + '"/></a>\
                <h4>' + video.name  + '</h4>';
        str = loadShortDescription(str, video);
        str += '</div>';
    }
    $("#popular-wrapper").html(str);
}

/*************************************************** PLAYLIST VIDEOS ***************************************************/
function getPlaylist(PL, query)
{
    if(PL==null) //set default playlist if none is assigned in url
    {
        PL=1649430486001
    }

    if(PL=="search") //If this is a Search Results Playlist
    {
        var call ="https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&video_fields=id,shortDescription,name,thumbnailURL,playsTotal&sort_by=CREATION_DATE:DESC&page_size=20&all=" + query + "&none=tag:hidden&sort_by=CREATION_DATE:DESC";
        addScriptTag("PlaylistJS", call, "getPlaylistResponse");
    }
    else //If this is a explicitly called playlist ID # or defaulted ID
    {
        var call ="https://api.brightcove.com/services/library?command=find_playlist_by_id&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&playlist_id="+ PL +"&video_fields=id,shortDescription,name,thumbnailURL,playsTotal&sort_by=CREATION_DATE:DESC&page_size=20&none=tag:hidden";
        addScriptTag("PlaylistJS", call, "getPlaylistResponse");
    }
}

function getPlaylistResponse(jsonData)
{
    if(jsonData["videos"]) //only called when explicit playlist ID # is used
    {
        if(!requestedVideo) //if no video ID # is passed in the URL
        {
            requestedVideo = jsonData["videos"][0].id;//define video to load -- make it first video in playlist
        }
        $("nav a[href='watch.htm#PL="+ PL +"']").parents('.top-nav').children('a.top').addClass("active");

        var str = "<h4>Playlist Videos:</h4>";
        for (var i=0 ; i<jsonData["videos"].length ; i++)
        {
            var video = jsonData["videos"][i];
            str += '<div>\
                    <a href="#" onClick="dynaLoad('+video.id+'); return false;" class="playlist-item-outer-wrapper">\
                    <div class="playlist-item-wrapper">\
                    <img src="' + video.thumbnailURL + '" class="pl-thumbnail"/>\
                    <div class="pl-description">\
                    <h6>' + video.name  + '</h6>';
            str = loadShortDescription(str, video);
            str += '<div style="clear:both"></div>\
                    </div>\
                    </div>\
                    </a>\
                    </div>';
        }
        $("#playlist-wrapper").html(str);
    }
    else if(jsonData["items"])//only called when search PL is used
    {
        if(!requestedVideo)
        {
            requestedVideo = jsonData["items"][0].id;
        }
        var str = '<div class="pl-inner-wrapper">\
                    <h4>Search Results:</h4>';
        for (var i=0; i<jsonData["items"].length; i++)
        {
            var video = jsonData["items"][i];
            str += '<div>\
                    <a href="#" onClick="dynaLoad('+video.id+');return false;" class="playlist-item-outer-wrapper">\
                    <div class="playlist-item-wrapper">\
                    <img src="' + video.thumbnailURL + '" class="pl-thumbnail"/>\
                    <div class="pl-description">\
                    <h6>' + video.name  + '</h6>';
            if(video.shortDescription !== null) //If Short Description is defined us it in label.
            {
                str += '<p>' + video.shortDescription  + '</p>'
            }
            str += '<div style="clear:both"></div>\
                    </div>\
                    </div>\
                    </a>\
                    </div>';
        }
        str += '</div>';
        $("#playlist-wrapper").html(str);
    }
}

/*************************************************** SEARCH RESULTS VIDEOS ***************************************************/
function getResults(query, page_number)
{
    var call ="https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&video_fields=id,shortDescription,name,videoStillURL,playsTotal&all=" + query + "&none=tag:hidden&page_size=8&get_item_count=true&page_number=" + page_number + "&sort_by=CREATION_DATE:DESC";
    addScriptTag("ResultsJS", call, "getResultsResponse");
}

function getResultsResponse(jsonData)
{
    passedVars = $.getUrlVars();
    query = passedVars.Q;
    var str = "<h1>Search Results</h1>"
    if(jsonData["items"].length<1)
    {
        str += '<h4 class="span12">No matching videos found.  Please try another search.</h4>\
                <div style="clear:both"></div>'
    }
    else
    {
        for (var i=0; i<jsonData["items"].length; i++)
        {
            var video = jsonData["items"][i];
            str += '<div>\
                    <a href="watch.htm#PL=search&V=' + video.id + '&Q=' + query + '" class="playlist-item-outer-wrapper row-fluid">\
                    <div class="playlist-item-wrapper">\
                    <img src="' + video.videoStillURL + '" class="pl-thumbnail"/>\
                    <div class="pl-description">\
                    <h4>' + video.name  + '</h4>';
            str = loadShortDescription(str, video);
            str += '<div style="clear:both"></div>\
                    </div>\
                    </div>\
                    </a>\
                    </div>';
        }
    }
    $("#results-wrapper").html(str);

    //Insert numbered page buttons for search results
    var pageCount=jsonData.total_count/8;
    if(pageCount>1)
    {
        if( pageCount>20 )
        {
            pageCount = 20;
        }
        var pageView = jsonData.page_number;
        pageView++;
        var str = '<div class="span12">';
        for ( var i=1 ; i<pageCount+1 ; i++ )
        {
            var classes="page-button";
            var next=i-1;
            if(i==(pageView)){classes+=" current"};
            str += '<a class="' +classes+ '" href="#" onClick="getResults(query,'+next+');return false;">' +i+ '</a>';
        }
        str += '</div>\
                <div class="clearfix"></div>';
        $("#results-wrapper").append(str);
    }
}

/*************************************************** RELATED VIDEOS ***************************************************/
function getRelatedVideos(requestedVideo, page_number)
{
    if($("#relatedVideos").length > 0)
    {
        $("#searchGen").remove();
    }
    var call='https://api.brightcove.com/services/library?command=find_video_by_id&video_id='+requestedVideo+'&video_fields=name,length,longDescription&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..'
    addScriptTag("videoByTitle", call, "getVideoByTitle");
    r_requestedVideo=requestedVideo;
    r_page_number=page_number;
}
function getVideoByTitle(jsonData)
{
    var videoTitle= jsonData.name;
    var call='https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..&page_size=1&all=display_name:'+videoTitle +'&exact=true';
    if($("#video-title").length > 0)
    {
        $("#video-title").html(videoTitle);
    };
    var LD = "";
    if(jsonData.longDescription)
    {
        LD += jsonData.longDescription;
    }
    $(".long-description").html(LD);
}
function loadRelatedVideos(requestedVideo, page_number, query)
{
    var call='https://api.brightcove.com/services/library?command=search_videos&token=73Yt-WegsBYcVsjincRL-3DqBRDPp60rFobnbA9MxNcnXamMLJOQNQ..' + query + '&video_fields=id,name,videoStillURL,playsTotal&sort_by=PLAYS_TOTAL:DESC&page_size=6&get_item_count=true&none=tag:hidden&page_number=' + page_number;
    addScriptTag("relatedVideos", call, "loadRelatedVideoResponse");
}
function loadRelatedVideoResponse(jsonData)
{
    var pageCount=jsonData.total_count/6;
    if(pageCount>5)
    {
        pageCount=5
    };
    pageCount=Math.ceil(pageCount);
    var pageView = jsonData.page_number;
    var prev;
    var next;
    if ( pageView == pageCount-1 )
    {
        next=0;
    }
    else
    {
        next=pageView+1;
    }
    if ( pageView == 0 )
    {
        prev = pageCount-1;
    }
    else
    {
        prev = pageView-1;
    }
    var str = "";
    if( pageCount > 1 )
    {
        str += '<div class="span1 previous-button"><a href="#" class="" onClick="getRelatedVideos(requestedVideo,'+ prev +'); return false;"><img src="/img/arrow-left2.png" alt="Next" /></a></div>\
                <div class="span10">\
                <div class="row-fluid">';
    }
    for ( var i=0 ; i<jsonData["items"].length ; i++)
    {
        var video = jsonData["items"][i];
        if(i%2==0)
        {
            str += '<div class="span2 related-video-wrapper">';
        }
        else
        {
            str += '<div class="span2 related-video-wrapper">';
        }
        str += '<a href="#" onClick="dynaLoad('+video.id+');return false;" ><img src="' + video.videoStillURL + '"/></a>\
                <p>' + video.name  + '</p>\
                </div>';
        if(i%2!=0)
        {
            str += '<div class="tile-fix"></div>';
        }
    }
    if(pageCount>1)
    {
        str += '</div>\
                </div>\
                <div class="span1 next-button"><a href="#" class="" onClick="getRelatedVideos(requestedVideo,'+ next +'); return false;"><img src="/img/arrow-right2.png" alt="Next" /></a></div>\
                <div class="previous-button-mbl tile-fix"><a href="#related-wrapper" class="" onClick="getRelatedVideos(requestedVideo,'+ prev +'); $(&quot;html, body&quot;).animate({scrollTop: $(&quot;#related-wrapper&quot;).parent().parent().offset().top}, 600); return false;"><img src="/img/arrow-left2.png" alt="Next" /></a></div>\
                <div class="next-button-mbl"><a href="#related-wrapper" class="" onClick="getRelatedVideos(requestedVideo,'+ next +'); $(&quot;html, body&quot;).animate({scrollTop: $(&quot;#related-wrapper&quot;).parent().parent().offset().top}, 600); return false;"><img src="/img/arrow-right2.png" alt="Next" /></a></div>';
    }
    str += '<div class="clearfix"></div>';
    $("#related-wrapper").html(str);
}

/*************************************************** VIDEO DESCRIPTIONS ***************************************************/
function loadShortDescription(string, requestedVideo)
{
    if(requestedVideo.shortDescription !== null) //If Short Description is defined us it in label.
    {
        string += '<p>' + requestedVideo.shortDescription  + '</p>'
        return string;
    }
}

function loadLongDescription(requestedVideo)
{

}

/*************************************************** LOADING VIDEO FUNCTIONS ***************************************************/
function dynaLoad(titleID)
{
    loadVideo(titleID);
    getRelatedVideos(titleID, 0);
    passedVars = $.getUrlVars();
    passedVars.V=titleID;
    var params="#";
    if(passedVars.PL)
    {
        params += "PL=" + passedVars.PL
    };
    if(passedVars.V)
    {
        params += "&V=" + passedVars.V
    };
    if(passedVars.Q)
    {
        params += "&Q=" + passedVars.Q
    };
    window.location.hash=params;
    $('html, body').animate(
    {
        scrollTop: $("#main-container").offset().top
    },600);

    var link= window.location;
    link = encodeURIComponent(link);
    var buttons="";
    buttons += '<a href="mailto:?Subject=I\'m sharing a video&body=I am sharing a video link with you:%0A%0A'+ link +'" class="mail-button">Email Link</a><p>Share this Video: </p>';
    //  buttons += '<a href="https://spiritube.web.spiritaero.com/player.htm#&V='+ requestedVideo +'" class="link-button">Direct Link</a>';
    $(".share-links").html(buttons);
}

/*************************************************** LAYOUT ADJUSTMENTS***************************************************/
function clipPlaylist()
{
    if($(window).width()>870)
    {
        var clipHeight = $("#myExperience").outerHeight();
        clipHeight +=-14;
        $("#playlist-wrapper").height(clipHeight);
    }
}


/***********************************************************************************************************************************/
/*************************************************** END OF FUNCTION DEFINITIONS ***************************************************/
/**************************************************** BEGIN OF CALLED FUNCTIONS ****************************************************/
/***********************************************************************************************************************************/

var query, requestedVideo, player, modVP, modExp, modCon;
var passedVars = $.getUrlVars();

$(document).ready(function()
{
    if(passedVars.Q)
    {
        query = passedVars.Q;
        var genQuery=query;
        genQuery=genQuery.replace(/\bSpirit\b/gi, '').replace(/\bBrightcove\b/gi, '').replace(/\bH.264\b/gi, '').replace(/\ba\b/gi, '').replace(/\bat\b/gi, '').replace(/\bthe\b/gi, '').replace(/\bof\b/gi, '').replace(/\bin\b/gi, '').replace(/\bon\b/gi, '').replace(/\bfor\b/gi, '').replace(/\bby\b/gi, '').replace(/\bto\b/gi, '').replace(/[^A-Za-z0-9]/g," ").replace(/\s+/gi, ' ');//ignore common words and remove double spaces.
        genQuery = query.replace(" ","&any=")
    }

    if(passedVars.V)
    {
        requestedVideo = passedVars.V.replace(/[^\d.]/g, "");
    }
    else if(!passedVars.V && !passedVars.PL && !passedVars.Q)
    {
        getFlagshipVideo();
    }

    if(passedVars.PL)
    {
        PL= passedVars.PL;
        getPlaylist(PL , query);
    }

    if($("#results-wrapper").length > 0)
    {
        getResults(query, 0);
    }
    $('nav li.top-nav').hover(function()
    {
        if($(window).width()>959)
        {
            $(this).children('.sub .sub').show()
        }
    },function()
    {
        if($(window).width()>959)
        {
            $(this).children('.sub .sub').hide()
        }
    });

/**************************************************** ON EVENT ACTIONS ****************************************************/

    // Cross browser placeholder text for search box
    $( ".search-box" ).focus( function(){
      if ( $(this).val() == "Search..." ){
        $(this).val("");
      }else if ( $(this).val() == "" ){
        $(this).val("Search...");
      }
    });

    $("#video-search").submit(function()
    {
        var query=$(this).children(".search-box").val();
        var genQuery=query;
        genQuery=genQuery.replace(/\bSpirit\b/gi, '').replace(/\bBrightcove\b/gi, '').replace(/\bH.264\b/gi, '').replace(/\ba\b/gi, '').replace(/\bat\b/gi, '').replace(/\bthe\b/gi, '').replace(/\bof\b/gi, '').replace(/\bin\b/gi, '').replace(/\bon\b/gi, '').replace(/\bfor\b/gi, '').replace(/\bby\b/gi, '').replace(/\bto\b/gi, '').replace(/[^A-Za-z0-9]/g," ").replace(/\s+/gi, ' ');//ignore common words and remove double spaces.
        genQuery = query.replace(" ","&all=")
        if(query=="Search..." || query==""){
            return false;
        }
        if($("#results-wrapper").length < 1)
        {
            window.open("search.htm#Q="+ query , "_self");
        }
        if($("#results-wrapper").length > 0)
        {
            getResults(genQuery, 0);
            window.location.hash="Q="+query;
        }
        return false;
    });

    $("section.wrapper h3 a.toggle").click(function()
    {
        $(this).toggleClass("minus");
        $(this).parent("h3").siblings("div").children(".toggle-content").stop().slideToggle();
    });

/**************************************************** LAYOUT ADJUSTMENTS ****************************************************/
    if($("#popular-wrapper").length > 0)
    {
        getTopVideos();
    }

    $(window).resize(function()
    {
        clipPlaylist();
        if($(window).width()>959)
        {
            $(".main").add(".side-main").show();
        }
    });

});
