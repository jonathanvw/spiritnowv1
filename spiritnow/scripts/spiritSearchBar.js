﻿function searchSubmit() {
  var query = encodeURIComponent($("#spirit-search").val());
  if (query == "Search..." || query == "") { return false; };
  var client = "Spirit";
  var output = "xml_no_dtd";
  var proxystylesheet = "Spirit";
  var site = "default_collection";
  //var url = encodeURIComponent("site:http://inside-rdu.web.spiritaero.com/");
  var sitesearch = "http://inside.spiritaero.com/spiritnow/news.aspx";
  var url = "";
    var request = "http://search.web.spiritaero.com/search?client=" + client + "&output=" + output + "&proxystylesheet=" + proxystylesheet + "&site=" + site + "&q=" + query + "&as_sitesearch="+ sitesearch;
    //var request = "http://search.web.spiritaero.com/search?client=Spirit&output=xml_no_dtd&proxystylesheet=Spirit&site=default_collection&q=" + query + "&as_oq=" + url;
  window.location = request;
}

function testForEnter(event) {
  if (event.keyCode == 13 || event.keyCode == 10) {
    event.cancelBubble = true;
    event.returnValue = false;
    searchSubmit();
  }
} 

$(document).ready(function () {
  $("searchbox-icon").click(function (event) {
    event.preventDefault();
    searchSubmit();
  });
});

// IE11 fix: prevent page's form from being submitted on enter key.  This allows the testForEnter() function to fire instead of posting back to the same page.
$(function() {
  $("form").on("keypress", "input[type=text], select, textarea", function(e) {
    if (e.which === 13 || e.which === 10 ) {
      e.preventDefault();
    }
  });
});
