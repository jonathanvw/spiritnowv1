﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Spiritnow.master" AutoEventWireup="true" CodeFile="news.aspx.cs" Inherits="_Spiritnow" Debug="true" %>
<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageContent" runat="server">
  <form id="formMain" runat="server" class="formMain">
      <!--
    <div class="navmenu navmenu-default navmenu-fixed-left nav-sections">
      <h3>News Sections:</h3>
      <ul class="content-by-section">
        <li><a href="#">Airbus</a></li>
        <li><a href="#">Boeing</a></li>
        <li><a href="#">Defense</a></li>
        <li><a href="#">Community</a></li>
        <li><a href="#">Leadership Messages</a></li>
      </ul>
      <h4>Content by location:</h4>
      <ul class="content-by-location">
        <li><a href="#">All Locaitons</a></li>
        <li><a href="#">Kansas</a></li>
        <li><a href="#">Oklahoma</a></li>
        <li><a href="#">North Carolina</a></li>
        <li><a href="#">France</a></li>
        <li><a href="#">Scotland</a></li>
        <li><a href="#">Malaysia</a></li>
      </ul>
    </div>-->
    <div class="jasny-canvas">
      <div class="container-fluid" style="max-width: 1400px !important">
        <div class="header">
          <div class="row">
            <div class="col-xs-12">
              <div class="text-right">
                <p><span class="date-wrapper"></span> | <span class="temperature-wrapper"></span> | <span class="stock-quote"></span></p>
              </div>
            </div>
          </div>
          <div class="row">



            <ul class="nav nav-pills">
              <!--<li>
                <a href="#" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".jasny-canvas" class="navbar-toggle">
                  <i class="fa fa-bars"></i> Sections
                </a>
              </li>-->
              <li>
                <div class="search-wrapper">
                  <div class="searchbox">
                    <input id ="spirit-search" type="search" placeholder="Search......" name="search" class="searchbox-input" onkeydown="testForEnter(event);" onkeyup="buttonUp(); return false;" required>
                    <button type="submit" class="searchbox-submit">
                      <span class="searchbox-icon"><i class="fa fa-search"></i></span>
                    </button>
                  </div>
                </div>
              </li>
              <li>
                <a href="/spiritnow.aspx/" class="mastLogo"><img src="/spiritnow/images/SpiritNow-logo.png" alt="SpiritNow" class="img-responsive" style="max-width: 300px;"></a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row"><!-- Top Content -->
          <div class="col-sm-9">
            <div class="row news-article-wrapper">
              <div class="brick-wall col-xs-12">
              <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                <asp:Label ID="storyOutputLabel" runat="server"></asp:Label>
                <div class="brick-item brick-item-w3">
                  <div class="gray-bg pad-thick">
                    <h2>Other Recent Stories</h2>
                    <ul>
                      <asp:Label ID="RecentStoriesOutput" runat="server"></asp:Label>
                    </ul>
                    <p class="text-right"><a href="/spiritnow/newsarchive.aspx">SEE MORE ></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <aside class="col-sm-3 sidebar">
            <section class="newslines-wrapper">
              <h3 class="pad-thin orange-bg white">Spirit Newslines</h3>
              <div class="newslines-list"></div>
              <p class="text-right"><a href="https://inside.spiritaero.com/news/newslines.aspx">SEE MORE ></a></p>
            </section>
            <section class="stock-wrapper"></section>
            <section class="press-release-wrapper">
              <h3 class="pad-thin blue-bg white">Press Releases by Company</h3>
              <ul class="list-inline links">
                <li><a href="#" data-rss="http://phx.corporate-ir.net/corporate.rss?c=196548&Rule=Cat=news~subcat=ALL" class="btn-pr active">Spirit</a></li>
                <li><a href="#" data-rss="http://boeing.mediaroom.com/news-releases-statements?pagetemplate=rss&category=786" class="btn-pr">Boeing</a></li>
                <li><a href="#" data-rss="http://www.airbus.com/newsevents/rss/all_airbus_families_news.xml" class="btn-pr">Airbus</a></li>
                <li><a href="#" data-rss="http://www.sikorsky.com/_layouts/feed.aspx?xsl=1&web=%2F&page=5dd004f7-0c34-4e45-8c8f-35855dd6cc25&wp=79c34605-4cf6-44b8-9b91-87a162f4c0b5&pageurl=%2FPages%2FAboutSikorsky%2FPressReleaseRSS%2Easpx" class="btn-pr">Sikorsky</a></li>
                <li><a href="#" data-rss="http://feed43.com/8831706846421816.xml" class="btn-pr">Bell</a></li>
                <li><a href="#" data-rss="https://www.mitsubishi.com/e/rss/index_e.xml" class="btn-pr">Mitsubishi</a></li>
                <li><a href="#" data-rss="http://www.rolls-royce.com/rss/press-releases" class="btn-pr">Rolls&#8209;Royce</a></li>
                <li><a href="#" data-rss="http://www.bombardier.com/services/rss.en.bca.xml" class="btn-pr">Bombardier</a></li>
              </ul>
              <div class="press-release-content"></div>
            </section>
            <section class="twitter-wrapper gray-bg pad-thin">
              <h3 class="pad-thin gray-darker">Twitter Feeds</h3>
              <ul class="list-inline links">
                <li><a href="#" data-twitterid="476078419180924928" class="btn-twitter active">@SpiritAero</a></li>
                <li><a href="#" data-twitterid="613818193648480257" class="btn-twitter">@Boeing</a></li>
                <li><a href="#" data-twitterid="613807356254810112" class="btn-twitter">@Airbus</a></li>
                <li><a href="#" data-twitterid="613818406899445760" class="btn-twitter">@Sikorsky</a></li>
                <li><a href="#" data-twitterid="613818665574748160" class="btn-twitter">@One_Bell</a></li>
              </ul>
              <div id="tweet-list" class="twitter-content pad-thin"></div>
            </section>
          </aside>
        </div><!-- End Top Content -->
        <hr class="thick">
        <h3>NEWS<span class="gray-lighter">FEEDS</span></h3>
        <hr>
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-4">
              <h4>Boeing:</h4>
              <div class="boeing-feed" data-src=""></div>
              <p class="text-right"><a href="http://boeing.mediaroom.com/news-releases-statements">SEE MORE ></a></p>
            </div>
            <div class="col-sm-4">
              <h4>Airbus:</h4>
              <div class="airbus-feed" data-src=""></div>
              <p class="text-right"><a href="http://www.airbus.com/newsevents/">SEE MORE ></a></p>
            </div>
            <div class="col-sm-4">
              <h4>Other Customers:</h4>
              <div class="other-feed" data-src=""></div>
              <p class="text-right"><a href="http://feeds.feedburner.com/otheraerospacenews">SEE MORE ></a></p>
            </div>
          </div>
        </div>

        <div class="footer">
          <p class="text-center"><a href="http://www.spiritaero.com">External Spirit Web</a> | <a href="http://inside.spiritaero.com">Inside Spirit Home</a> | <a href="mailto:wchspiritnewspaper@spiritaero.com">Contact SpiritNow</a></p>
        </div>

      </div>
  </div>
</form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FootScripts" runat="server">
  <script type="text/javascript">
      var customPageName = "/spiritnow/" + $(".news-article-wrapper h1").first().html();
      (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date(); a = s.createElement(o),
      m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-39758253-2', 'spiritaero.com');
      ga('send', 'pageview', customPageName);
  </script>
</asp:Content>
