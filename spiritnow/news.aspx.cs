﻿using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.API.Content;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Framework.UI.Controls;
using Ektron.Cms.Framework.UI.Controls.Views;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Widget;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class _Spiritnow : PageBuilder
{
  public long blogID = 4294967468;
  public long numPostVisible = 0;
  public bool displayTitle = false;
  public bool useTeaser = true;
  public bool usePagination = false;
  public bool showAddPost = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        Ektron.Cms.API.Content.Content ContentAPI = new Ektron.Cms.API.Content.Content();

        // Setup Blog Posts
        if (blogID <= 0)
        {
            //BlogOutputLabel.Text = "<p>Please click the \"edit\" icon to setup the blog post options</p>";
            return;
        }
        string htmlOutput;
        long postID;
        String str_postID = Request.QueryString["post"];
        if (long.TryParse(str_postID, out postID))
        {
            //postID now contains your int value
        }
        else
        {
            //str_postID contained something else, i.e. not int
            postID = 0;
        }

        Ektron.Cms.API.Content.Blog b = new Blog();
        DateTime rightNow = DateTime.Now;
        Ektron.Cms.BlogData bd = b.GetBlog(blogID, rightNow.ToShortDateString(), 0, 1033);
        BlogPostData[] postArray = bd.Content;
        //Remove all posts that are not live
        postArray = Array.FindAll(postArray, x => isLivePost(x));

        if( postID > 0){
            // If building single post view
            htmlOutput = buildSinglePost(bd, blogID, postID);
            storyOutputLabel.Text = htmlOutput;
        }else{
            // TODO setup default article to display if no valide postID is provided
        }
        htmlOutput = buildRecentStoriesList(bd, postArray, numPostVisible);
        RecentStoriesOutput.Text = htmlOutput;
    }
    protected bool EnableLink(String LinkType, string Url)
    {
        bool retVal = true;
        switch (LinkType.ToLower())
        {
            case "submenu":
                if ((Url == "") || (Url == "/"))
                {
                    retVal = false;
                }
                break;
        }
        return retVal;
    }
    public override void Error(string message)
    {
        jsAlert(message);
    }
    public override void Notify(string message)
    {
        jsAlert(message);
    }
    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
    static bool isLivePost(BlogPostData d)
    {
            //Check if post is active at the time of viewing
            DateTime rightNow = DateTime.Now;
            int postBeginCheck = DateTime.Compare(rightNow, d.StartDate);
            int postEndCheck= DateTime.Compare(rightNow, d.EndDate);
            if (postBeginCheck >= 0 && postEndCheck <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
    }

    private string buildRecentStoriesList(BlogData bd, BlogPostData[] PostData, long numPost)
    {
        long blogLength = PostData.Length;
        double totalPages = Math.Ceiling(Convert.ToDouble(blogLength) / Convert.ToDouble(numPost));

        // Check if we are in bounds of available posts
        numPost = (numPost > blogLength) ? blogLength : numPost;

        StringBuilder htmlOut = new StringBuilder();

        int currPage;
        if (!int.TryParse(Request.QueryString["page"], out currPage))
        {
            currPage = 1;
        }
        long longCurrPage = Convert.ToInt64(currPage);
        long begin = 0 + numPostVisible;
        long end = begin + 10;

        // Check and make sure we are in bounds of available posts
        end = (end > blogLength - 1) ? blogLength - 1 : end;

        // Gets the Ektron Alias name for url
        CommonAliasManager m = new CommonAliasManager();
        long pageID = Convert.ToInt64(Request.QueryString["pageid"]);

        for( long i = begin ; i <= end ; i++ )
        {
            //string url = m.GetContentAlias(pageID);
            string url = "";
            BlogPostData posting = PostData[i];
            url += "?&post=";
            url += posting.Id.ToString();

            htmlOut.Append("<li><a href='/spiritnow/news.aspx" + url + "'>" + posting.Title.ToString() + "</a></li>");
        }
        return htmlOut.ToString();
    }

    private string buildSinglePost(BlogData bd, long blogID, long postID)
    {
        StringBuilder htmlOut = new StringBuilder();
        htmlOut.Append("<div class=\"brick-item brick-item-w3\">");
        Ektron.Cms.API.Content.Blog bapi = new Ektron.Cms.API.Content.Blog();

            BlogPostData posting = bapi.GetPostbyID(postID);
            //string url = HttpContext.Current.Request.Url.AbsoluteUri + "?&post=" + posting.Id.ToString();
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            htmlOut.Append("<h1>" + posting.Title.ToString() + "</h1>");
            htmlOut.Append("<p style='margin: -0.75em 0 0.25em 0; font-size: 14px; font-family: Helvetica,Arial,sans-serif; font-weight: lighter; vertical-align:top;' class='gray-lighter'> " + posting.DateCreated.ToString("MM/dd/yyyy") + "</p>");
            htmlOut.Append("<p>" + posting.Html.ToString() + "</p>");
            htmlOut.Append("<hr class='clearfix' />");

        htmlOut.Append("</div>");
        return htmlOut.ToString();
    }
}
