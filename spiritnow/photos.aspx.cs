﻿using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.API.Content;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Framework.UI.Controls;
using Ektron.Cms.Framework.UI.Controls.Views;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Widget;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class _Spiritnow : PageBuilder
{
  public long blogID = 4294967468;
  public long numPostVisible = 5;
  public bool displayTitle = false;
  public bool useTeaser = true;
  public bool usePagination = false;
  public bool showAddPost = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        Ektron.Cms.API.Content.Content ContentAPI = new Ektron.Cms.API.Content.Content();
        buildSpiritPhotoGallery();
    }
    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        // Adds script to top of header before the Ektron.js script
        // Page.Header.Controls.AddAt(0, new LiteralControl("<script src='/js/domain.js'></script>"));
    }
    protected bool EnableLink(String LinkType, string Url)
    {
        bool retVal = true;
        switch (LinkType.ToLower())
        {
            case "submenu":
                if ((Url == "") || (Url == "/"))
                {
                    retVal = false;
                }
                break;
        }
        return retVal;
    }
    public override void Error(string message)
    {
        jsAlert(message);
    }
    public override void Notify(string message)
    {
        jsAlert(message);
    }
    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }
    static bool isLivePost(BlogPostData d)
    {
            //Check if post is active at the time of viewing
            DateTime rightNow = DateTime.Now;
            int postBeginCheck = DateTime.Compare(rightNow, d.StartDate);
            int postEndCheck= DateTime.Compare(rightNow, d.EndDate);
            if (postBeginCheck >= 0 && postEndCheck <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
    }
    private void buildSpiritPhotoGallery()
    {
      try
        {
            LibraryManager manager = new LibraryManager();
            LibraryCriteria criteria = new LibraryCriteria();
            // SNN News folder id is 4294967468
            criteria.AddFilter(LibraryProperty.ParentId, CriteriaFilterOperator.EqualTo, 4294967468);
            criteria.AddFilter(LibraryProperty.ContentType, CriteriaFilterOperator.EqualTo, 7);
            //criteria.AddFilter(LibraryProperty., CriteriaFilterOperator.EqualTo, 7);
            // Reference: https://developer.ektron.com/Templates/CodeLibraryDetail.aspx?id=2226&blogid=116
            criteria.OrderByField = Ektron.Cms.Common.LibraryProperty.CreatedDate;
            criteria.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Descending;
            Ektron.Cms.PagingInfo pagingInfo = new Ektron.Cms.PagingInfo();
            //sets the page of results you want
            pagingInfo.CurrentPage = 1;
            //how many items to return per page
            pagingInfo.RecordsPerPage = 200;
            criteria.PagingInfo = pagingInfo;
            List<LibraryData> photoItems = manager.GetList(criteria);
            //photoItems = photoItems.GetRange(0,4);
            uxLibraryDataListView.DataSource = photoItems;
            uxLibraryDataListView.DataBind();
            //GridView1.DataSource = photoItems;
            //GridView1.DataBind();
        }
        catch (Exception ex)
        {
          Response.Write(ex.Message);
        }
    }
}
