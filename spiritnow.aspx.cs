﻿using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.API.Content;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Framework.UI.Controls;
using Ektron.Cms.Framework.UI.Controls.Views;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Widget;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class _Spiritnow : PageBuilder
{
  public long blogID = 4294967468;
  public long numPostVisible = 5;
  public bool displayTitle = false;
  public bool useTeaser = true;
  public bool usePagination = false;
  public bool showAddPost = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        Ektron.Cms.API.Content.Content ContentAPI = new Ektron.Cms.API.Content.Content();

        // Setup Blog Posts
        if (blogID <= 0)
        {
            BlogOutputLabel.Text = "<p>Please click the \"edit\" icon to setup the blog post options</p>";
            return;
        }
        string htmlOutput;
        long postID;
        String str_postID = Request.QueryString["post"];
        if (long.TryParse(str_postID, out postID))
        {
            //postID now contains your int value
        }
        else
        {
            //str_postID contained something else, i.e. not int
            postID = 0;
        }

        Ektron.Cms.API.Content.Blog b = new Ektron.Cms.API.Content.Blog();
        DateTime rightNow = DateTime.Now;
        Ektron.Cms.BlogData bd = b.GetBlog(blogID, rightNow.ToShortDateString(), 15, 1033);
        BlogPostData[] postArray = bd.Content;
        //postArray = Array.FindAll(postArray, x => isLivePost(x));

        //Remove all posts that are not live
        BlogPostData[] FeaturedPostArray = Array.FindAll(postArray, x => isFeaturedPost(x) && isLivePost(x));
        BlogPostData[] basicPostArray = Array.FindAll(postArray, x => !isFeaturedPost(x) && isLivePost(x));

        postArray = new BlogPostData[FeaturedPostArray.Length + basicPostArray.Length ];
        FeaturedPostArray.CopyTo(postArray, 0);
        basicPostArray.CopyTo(postArray, FeaturedPostArray.Length);
        if( postID > 0){
            // If building single post view
            htmlOutput = buildSinglePost(displayTitle, bd, blogID, postID);
        }else{
            // If building blog posts view
            htmlOutput = buildBlogPostBricks(bd, postArray, numPostVisible, b);
            htmlOutput += "<div class='brick-item brick-item-w1' style='border:0px; padding: 0;'><a href='/storysubmission.aspx' target='_blank' class='btn btn-sm btn-primary btn-block'>Submit Your Story Idea</a></div>";
        }
        BlogOutputLabel.Text = htmlOutput;

        htmlOutput = buildRecentStoriesList(bd, postArray, numPostVisible);
        RecentStoriesOutput.Text = htmlOutput;
        buildSpiritPhotos();
        buildCarousel();
    }
    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        // Adds script to top of header before the Ektron.js script
        // Page.Header.Controls.AddAt(0, new LiteralControl("<script src='/js/domain.js'></script>"));
    }
    protected bool EnableLink(String LinkType, string Url)
    {
        bool retVal = true;
        switch (LinkType.ToLower())
        {
            case "submenu":
                if ((Url == "") || (Url == "/"))
                {
                    retVal = false;
                }
                break;
        }
        return retVal;
    }
    public override void Error(string message)
    {
        jsAlert(message);
    }
    public override void Notify(string message)
    {
        jsAlert(message);
    }
    public void jsAlert(string message)
    {
        Literal lit = new Literal();
        lit.Text = "<script type=\"\" language=\"\">{0}</script>";
        lit.Text = string.Format(lit.Text, "alert('" + message + "');");
        Form.Controls.Add(lit);
    }

    static bool isFeaturedPost(BlogPostData posting)
    {
            Ektron.Cms.API.Content.Blog blog = new Ektron.Cms.API.Content.Blog();
            Ektron.Cms.BlogPostData bpd = blog.GetBlogPostData(posting.Id);
            Ektron.Cms.ContentData postData = blog.GetPost(posting.Id, ref bpd);
            int count = 0;
            foreach(Ektron.Cms.ContentMetaData contentMetaData in postData.MetaData)
            {
                if (contentMetaData.Name == "keywords" && contentMetaData.Text== "featured"){
                  count++;
                }
            }
            //Check if post is has keyword (metatag) = "featured"
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
    }
    static bool isLivePost(BlogPostData d)
    {
            //Check if post is active at the time of viewing
            DateTime rightNow = DateTime.Now;
            int postBeginCheck = DateTime.Compare(rightNow, d.StartDate);
            int postEndCheck= DateTime.Compare(rightNow, d.EndDate);
            if (postBeginCheck >= 0 && postEndCheck <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
    }
    private string buildRecentStoriesList(BlogData bd, BlogPostData[] PostData, long numPost)
    {
        long blogLength = PostData.Length;
        double totalPages = Math.Ceiling(Convert.ToDouble(blogLength) / Convert.ToDouble(numPost));

        // Check if we are in bounds of available posts
        numPost = (numPost > blogLength) ? blogLength : numPost;

        StringBuilder htmlOut = new StringBuilder();

        int currPage;
        if (!int.TryParse(Request.QueryString["page"], out currPage))
        {
            currPage = 1;
        }
        long longCurrPage = Convert.ToInt64(currPage);
        long begin = 0 + numPostVisible;
        long end = begin + 10;

        // Check and make sure we are in bounds of available posts
        end = (end > blogLength - 1) ? blogLength - 1 : end;

        // Gets the Ektron Alias name for url
        CommonAliasManager m = new CommonAliasManager();
        long pageID = Convert.ToInt64(Request.QueryString["pageid"]);

        for( long i = begin ; i <= end ; i++ )
        {
            //string url = m.GetContentAlias(pageID);
            string url = "";
            BlogPostData posting = PostData[i];
            url += "?&post=";
            url += posting.Id.ToString();

            htmlOut.Append("<li><a href='/spiritnow/news.aspx" + url + "'>" + posting.Title.ToString() + "</a></li>");
        }
        return htmlOut.ToString();
    }
    private string buildBlogPostBricks(BlogData bd, BlogPostData[] PostData, long numPost, Ektron.Cms.API.Content.Blog b)
    {
        long blogLength = PostData.Length;
        double totalPages = Math.Ceiling(Convert.ToDouble(blogLength) / Convert.ToDouble(numPost));

        // Check if we are in bounds of available posts
        numPost = (numPost > blogLength) ? blogLength : numPost;

        StringBuilder htmlOut = new StringBuilder();
        // If user is logged in, show them the Add blog post link
        if (Ektron.Cms.Framework.Context.UserContextService.Current.IsLoggedIn && showAddPost )
        {
            htmlOut.Append("<a href=\"/tdcUpdates.aspx?pageid=4722#\" onclick=\"EkTbWebMenuPopUpWindow('/WorkArea/editarea.aspx?ContType=1&amp;LangType=1033&amp;id=" + blogID.ToString() + "&amp;type=add&amp;xid=0', 'Add','900','580', 1, 1);return false;\"><br>Add Post<br><br></a>");
        }

        int currPage;
        if (!int.TryParse(Request.QueryString["page"], out currPage))
        {
            currPage = 1;
        }
        long longCurrPage = Convert.ToInt64(currPage);
        long begin = Convert.ToInt64((longCurrPage * numPost) - numPost);
        long end = Convert.ToInt64((longCurrPage * numPost) - 1);

        // Check and make sure we are in bounds of available posts
        end = (end > blogLength - 1) ? blogLength - 1 : end;

        // Gets the Ektron Alias name for url
        CommonAliasManager m = new CommonAliasManager();
        long pageID = Convert.ToInt64(Request.QueryString["pageid"]);

        for( long i = begin ; i <= end ; i++ )
        {
            string url = m.GetContentAlias(pageID);
            BlogPostData posting = PostData[i];
            Ektron.Cms.BlogPostData bpd = b.GetBlogPostData(posting.Id);
            Ektron.Cms.ContentData thispostdata = b.GetPost(posting.Id, ref bpd);

            url += "spiritnow/news.aspx";
            url += "?&post=";
            url += posting.Id.ToString();


            //Alternates 2,1,2,1,2,3...//
            htmlOut.Append("<div class=\"brick-item brick-item-w" + (((i+1)%2) + 1 + Convert.ToInt32(!Convert.ToBoolean(((Math.Pow(i,i)+5)%5 + 0)))*2 ) + "\">");
            //htmlOut.Append("<div class=\"brick-item brick-item-w3\">");
            /*
            foreach (Ektron.Cms.ContentMetaData contentMetaData2 in thispostdata.MetaData)
            {
                if (contentMetaData2.Name == "keywords" && contentMetaData2.Text== "featured"){
                  htmlOut.Append("<h2 class='orange'>FEATURED!</h2>");
                }
            }
            */
            htmlOut.Append("<h2><a href='/" + url + "'>" + posting.Title.ToString() + "</a></h2>");
            //htmlOut.Append("<p style='margin: -0.75em 0 0.25em 0; font-size: 14px; font-family: Helvetica,Arial,sans-serif; font-weight: lighter; vertical-align:top;' class='gray-lighter'> " + posting.DateCreated.ToString("MM/dd/yyyy") + "</p>");
            //If use teaser option is on, and if the teaser for the post isn't empty
            if (useTeaser && posting.Teaser.ToString().Replace("<br class=\"aloha-cleanme\">", "").Trim().Length > 0)
            {
                string pattern = @".*<img.*src=""([^""]*).*>.*";
                Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
                Match regM = r.Match(posting.Html);
                string firstImg = Convert.ToString( regM.Groups[1] );
                if ( firstImg.Length > 0 ){
                  firstImg = "<img src='" + firstImg + "' class='teaserImg' />";
                }
                htmlOut.Append("<div class='teaser-post'>"+ firstImg + posting.Teaser.ToString() + "</div>");
                htmlOut.Append("<p><a href='/" + url + "' class='pull-right'>Read More ></a></p>");
            }
            //Else display the entire post's content
            else
            {
                htmlOut.Append("<p>" + posting.Html.ToString() + "</p>");
            }
            //htmlOut.Append("<p>" + posting.Teaser.ToString() + "</p>");
            //htmlOut.Append("<!--Id: " + posting.Id.ToString() + "-->");
            htmlOut.Append("</div>");
        }
        // Add pagination links here
        string pageUrl = "/" + m.GetContentAlias(pageID);
        double pageLimit = 9;
        if (totalPages > 1 && usePagination )
        {
            double startPage;
            double pageLimitFloor = Math.Floor(pageLimit/2);

            htmlOut.Append("<div class=\"pagination\">Page: ");
            if(currPage > (pageLimit - pageLimitFloor)) {
                // set the prevLink button with a minumum of page 1
                string prevLink = currPage - pageLimit < 1 ? "1" : (currPage - pageLimit).ToString();
                // Insert button to view next ten pages
                htmlOut.Append("<a class=\"btn btn-defautl\" href=\"" + pageUrl + "?page=" + prevLink + "\"><<</a>");
            }
            if(currPage > (totalPages - pageLimitFloor)){
                startPage = (totalPages - pageLimit) >0 ? (totalPages-pageLimit) : 1 ;
            }else if(currPage>=(pageLimit/2)){
                startPage = currPage - pageLimitFloor;
                pageLimit -= pageLimitFloor;
            }else{
                startPage = 1;
                pageLimit -= currPage -1;
            }
            for (double j = startPage; j <= totalPages && j < (currPage + pageLimit) ; j++)
            {
                // Disable current page link in pageination links
                if(j==Convert.ToDouble(currPage)){
                    htmlOut.Append("<a class=\"btn btn-default active\" href=\"" + pageUrl + "?page=" + j + "\" class=\"active\" onClick=\"return false;\">" + j + "</a>");
                }else{
                    htmlOut.Append("<a class=\"btn btn-default\" href=\"" + pageUrl + "?page=" + j + "\">" + j + "</a>");
                }
            }
            if(currPage < (totalPages - pageLimit +1)) {
                // set the prevLink button with a minumum of page 1
                string nextLink = (currPage + pageLimit).ToString();
                // Insert button to view next ten pages
                htmlOut.Append("<a class=\"btn btn-default\" href=\"" + pageUrl + "?page=" + nextLink + "\">>></a>");
            }
            htmlOut.Append("</div>");
        }
        return htmlOut.ToString();
    }
    private string buildBlogPosts(BlogData bd, BlogPostData[] PostData, long numPost)
    {
        long blogLength = PostData.Length;
        double totalPages = Math.Ceiling(Convert.ToDouble(blogLength) / Convert.ToDouble(numPost));

        // Check if we are in bounds of available posts
        numPost = (numPost > blogLength) ? blogLength : numPost;

        StringBuilder htmlOut = new StringBuilder();
        if (displayTitle)
        {
            htmlOut.Append("<h1>");
            htmlOut.Append(bd.Title.ToString());
            htmlOut.Append("</h1>");
        }
        // If user is logged in, show them the Add blog post link
        if (Ektron.Cms.Framework.Context.UserContextService.Current.IsLoggedIn && showAddPost )
        {
            htmlOut.Append("<a href=\"/tdcUpdates.aspx?pageid=4722#\" onclick=\"EkTbWebMenuPopUpWindow('/WorkArea/editarea.aspx?ContType=1&amp;LangType=1033&amp;id=" + blogID.ToString() + "&amp;type=add&amp;xid=0', 'Add','900','580', 1, 1);return false;\"><br>Add Post<br><br></a>");
        }

        int currPage;
        if (!int.TryParse(Request.QueryString["page"], out currPage))
        {
            currPage = 1;
        }
        long longCurrPage = Convert.ToInt64(currPage);
        long begin = Convert.ToInt64((longCurrPage * numPost) - numPost);
        long end = Convert.ToInt64((longCurrPage * numPost) - 1);

        // Check and make sure we are in bounds of available posts
        end = (end > blogLength - 1) ? blogLength - 1 : end;

        // Gets the Ektron Alias name for url
        CommonAliasManager m = new CommonAliasManager();
        long pageID = Convert.ToInt64(Request.QueryString["pageid"]);

        for( long i = begin ; i <= end ; i++ )
        {
            string url = m.GetContentAlias(pageID);
            BlogPostData posting = PostData[i];
            url += "?&post=";
            url += posting.Id.ToString();

            htmlOut.Append("<div class=\"entry\">");
            htmlOut.Append("<h2><a href='/" + url + "'>" + posting.Title.ToString() + "</a></h2>");
            //If use teaser option is on, and if the teaser for the post isn't empty
            if (useTeaser && posting.Teaser.ToString().Replace("<br class=\"aloha-cleanme\">", "").Trim().Length > 0)
            {
                htmlOut.Append("<div class='teaser-post'>" + posting.Teaser.ToString() + "</div>");
                htmlOut.Append("<p><a href='/" + url + "' class='pull-right'>Read More >>></a></p>");
            }
            //Else display the entire post's content
            else
            {
                htmlOut.Append("<p>" + posting.Html.ToString() + "</p>");
            }
            //htmlOut.Append("<p>" + posting.Teaser.ToString() + "</p>");
            //htmlOut.Append("<!--Id: " + posting.Id.ToString() + "-->");
            htmlOut.Append("</div>");
            htmlOut.Append("<hr class=\"clearfix\" />");
        }
        // Add pagination links here
        string pageUrl = "/" + m.GetContentAlias(pageID);
        double pageLimit = 9;
        if (totalPages > 1 && usePagination )
        {
            double startPage;
            double pageLimitFloor = Math.Floor(pageLimit/2);

            htmlOut.Append("<div class=\"pagination\">Page: ");
            if(currPage > (pageLimit - pageLimitFloor)) {
                // set the prevLink button with a minumum of page 1
                string prevLink = currPage - pageLimit < 1 ? "1" : (currPage - pageLimit).ToString();
                // Insert button to view next ten pages
                htmlOut.Append("<a class=\"btn btn-defautl\" href=\"" + pageUrl + "?page=" + prevLink + "\"><<</a>");
            }
            if(currPage > (totalPages - pageLimitFloor)){
                startPage = (totalPages - pageLimit) >0 ? (totalPages-pageLimit) : 1 ;
            }else if(currPage>=(pageLimit/2)){
                startPage = currPage - pageLimitFloor;
                pageLimit -= pageLimitFloor;
            }else{
                startPage = 1;
                pageLimit -= currPage -1;
            }
            for (double j = startPage; j <= totalPages && j < (currPage + pageLimit) ; j++)
            {
                // Disable current page link in pageination links
                if(j==Convert.ToDouble(currPage)){
                    htmlOut.Append("<a class=\"btn btn-default active\" href=\"" + pageUrl + "?page=" + j + "\" class=\"active\" onClick=\"return false;\">" + j + "</a>");
                }else{
                    htmlOut.Append("<a class=\"btn btn-default\" href=\"" + pageUrl + "?page=" + j + "\">" + j + "</a>");
                }
            }
            if(currPage < (totalPages - pageLimit +1)) {
                // set the prevLink button with a minumum of page 1
                string nextLink = (currPage + pageLimit).ToString();
                // Insert button to view next ten pages
                htmlOut.Append("<a class=\"btn btn-default\" href=\"" + pageUrl + "?page=" + nextLink + "\">>></a>");
            }
            htmlOut.Append("</div>");
        }
        return htmlOut.ToString();
    }
    private string buildSinglePost(bool displayTitle, BlogData bd, long blogID, long postID)
    {
        StringBuilder htmlOut = new StringBuilder();
        htmlOut.Append("<div class=\"entry\">");
        if (displayTitle)
        {
            htmlOut.Append("<h1>");
            htmlOut.Append(bd.Title.ToString());
            htmlOut.Append("</h1>");
        }

        Ektron.Cms.API.Content.Blog bapi = new Ektron.Cms.API.Content.Blog();

            BlogPostData posting = bapi.GetPostbyID(postID);
            //string url = HttpContext.Current.Request.Url.AbsoluteUri + "?&post=" + posting.Id.ToString();
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            htmlOut.Append("<h2>" + posting.Title.ToString() + "</h2>");
            htmlOut.Append("<p>" + posting.Html.ToString() + "</p>");
            htmlOut.Append("<hr class='clearfix' />");

        htmlOut.Append("</div>");
        return htmlOut.ToString();
    }
    private void buildCarousel()
    {
      try
        {
          // Spiritnow Slide Images folder id is 27917287536
          //long folderID = 27917287536;
          long folderID = 27917287689;
          Ektron.Cms.API.Content.Content cntApi = new Ektron.Cms.API.Content.Content();
          Ektron.Cms.API.Metadata metaDataAPI = new Ektron.Cms.API.Metadata();
          ContentData[] cdAssetList = cntApi.GetChildContent(folderID, true);

          /* var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize( cdAssetList );
          Response.Write(json);*/

          // Reference: http://webguro.blogspot.com/2010/10/ektron-api-to-retrieve-dms-document-and.html
          /*
          foreach (ContentData cdAsset in cdAssetList)
          {
            Response.Write("Title = "+cdAsset.Title + "<br />Path = "+ cdAsset.Quicklink + "<br />Teaser = " + cdAsset.Teaser + "<br/><br/>");
              //Now getting metadata for DMS Document
              CustomAttributeList calist = metaDataAPI.GetContentMetadataList(cdAsset.Id);

            //foreach (Ektron.Cms.CustomAttribute ca in calist.AttributeList)
            //{
              //Response.Write(ca.Value);
            //}
          }
          */
          // Reference: http://stackoverflow.com/questions/632570/cast-received-object-to-a-listobject-or-ienumerableobject
          var result = ((IEnumerable)cdAssetList).Cast<object>().ToList();
          //Response.Write( Convert.ToString( result.GetType() ) );

          CarouselListView.DataSource = result;
          CarouselListView.DataBind();
        }
        catch (Exception ex)
        {
          Response.Write(ex.Message);
        }
    }
    private void buildSpiritPhotos()
    {
      try
        {
            LibraryManager manager = new LibraryManager();
            LibraryCriteria criteria = new LibraryCriteria();
            // SNN News folder id is 4294967468
            criteria.AddFilter(LibraryProperty.ParentId, CriteriaFilterOperator.EqualTo, 4294967468);
            criteria.AddFilter(LibraryProperty.ContentType, CriteriaFilterOperator.EqualTo, 7);
            //criteria.AddFilter(LibraryProperty., CriteriaFilterOperator.EqualTo, 7);
            // Reference: https://developer.ektron.com/Templates/CodeLibraryDetail.aspx?id=2226&blogid=116
            criteria.OrderByField = Ektron.Cms.Common.LibraryProperty.CreatedDate;
            criteria.OrderByDirection = Ektron.Cms.Common.EkEnumeration.OrderByDirection.Descending;

            List<LibraryData> photoItems = manager.GetList(criteria);
            photoItems = photoItems.GetRange(0,4);
            uxLibraryDataListView.DataSource = photoItems;
            uxLibraryDataListView.DataBind();
            //GridView1.DataSource = photoItems;
            //GridView1.DataBind();
        }
        catch (Exception ex)
        {
          Response.Write(ex.Message);
        }
    }
}
